#include "vecteur.h"
#include <stdlib.h>

vecteur allouer_vecteur(int taille) {
    vecteur v = {taille, malloc(sizeof(double)*taille)};
    return v;
}

void liberer_vecteur(vecteur v) {
 free(v.donnees);
 }

int est_vecteur_invalide(vecteur v) {
    int resultat = 0;
    if(v.taille<0){
      resultat=1;
    }
    return resultat;
}

double *acces_vecteur(vecteur v, int i) {
    double *resultat = &v.donnees[i];
    return resultat;
}

int taille_vecteur(vecteur v) {
    int resultat = v.taille;
    return resultat;
}
