#include "vecteur_dynamique.h"
#include <stdlib.h>

vecteur allouer_vecteur(int taille) {
    vecteur v = malloc(sizeof(struct donnees_vecteur));
    v->taille=taille;
    v->donnees = malloc(sizeof(double)*taille);
    return v;
}

void liberer_vecteur(vecteur v) {
  free(v->donnees);
  free(v);
}

int est_vecteur_invalide(vecteur v) {
    int resultat = (v->taille <0? 1:0);
    return resultat;
}

double *acces_vecteur(vecteur v, int i) {
    if (i<0){
      return NULL;
    }else if (i< v->taille){
      return v->donnees + i;
    }else {
      v->donnees = realloc(v->donnees, sizeof(double *) * (i+1));
      v->taille = i;
      return v->donnees+i;
    }
}

int taille_vecteur(vecteur v) {
    int resultat = v->taille;
    return resultat;
}
