#include "memory_operations.h"
#include <stdlib.h>

typedef	unsigned char	BYTE ;

void *my_memcpy(void *dst, const void *src, size_t len) {
    BYTE * dst_byte = (BYTE *) dst;
    BYTE * src_byte = (BYTE *) src;
    for (int i = 0; i < len; i++)
      *(dst_byte+i) = *(src_byte+i);

    return dst;
}

void *my_memmove(void *dst, const void *src, size_t len) {
    BYTE * dst_byte = (BYTE *) dst;
    BYTE * src_byte = (BYTE *) src;
    if (dst > src){
      for (int i = len; i > 0; i--)
        dst_byte[i] = src_byte[i];
    }else if ( dst < src){
      for (int i = 0; i < len; i++)
        dst_byte[i] = src_byte[i];
    }
    return dst;
}

int is_little_endian() {
    int x = 1; //si 4 octets sous la forme 0x00000001
    BYTE * y = (BYTE *)&x; //BYTE => 1 octet; on caste l'entier en un tableau de d'octets
    if (*(y+3) == 0x01){
      return 0;
    }else{
      return 1;
    }
}

int reverse_endianess(int value) {
    BYTE * bytes_value = (BYTE *)&value;

    BYTE * b1;
    BYTE * b2;
    BYTE b_save;

    for (int i = 0; i < 2; i++){
      b1 = bytes_value + i;
      b2 = bytes_value+ (3 - i);
      b_save = *b1;
      *b1 = *b2;
      *b2 = b_save;
    }

    return value;
}
